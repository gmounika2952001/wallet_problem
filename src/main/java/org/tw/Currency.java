package org.tw;

public class Currency {

    public static class BaseClass {
        static final BaseClass dollar = new BaseClass(1);
        static final BaseClass rupees = new BaseClass(1);
        static final BaseClass dollarToRupees = new BaseClass(74.85);
        static final BaseClass rupeesToDollar = new BaseClass(1 / 74.85);

        private final double baseValue;

        BaseClass(double baseValue) {
            this.baseValue = baseValue;
        }
    }

    private final double values;
    private double moneyInWallet = 0;
    private final BaseClass unit;

    public Currency(double values, BaseClass unit) {
        this.values = values;
        this.unit = unit;
    }

    public static Currency dollar(double value) {
        return new Currency(value, BaseClass.dollar);
    }

    public static Currency rupeesToDollar(double value) {
        return new Currency(value, BaseClass.rupeesToDollar);
    }

    public static Currency rupees(double value) {
        return new Currency(value, BaseClass.rupees);
    }

    public static Currency dollarToRupees(double value) {
        return new Currency(value, BaseClass.dollarToRupees);
    }

    public Currency moneyAddedToWalletInRupees() {
        moneyInWallet = moneyInWallet + this.values;
        return new Currency(moneyInWallet, BaseClass.rupees);
    }

    public Currency add(Currency object) {
        double objectUnitConversion = object.convertToBase();
        return new Currency(this.values + objectUnitConversion, this.unit);
    }

    public Currency add1(Currency oneDollarObject, Currency hundredFortyNinePointSevenRupeesObject) {
        double objectUnitConversion1=this.convertToBase();
        double objectUnitConversion2= hundredFortyNinePointSevenRupeesObject.convertToBase();
        return new Currency(objectUnitConversion1+oneDollarObject.values + objectUnitConversion2, oneDollarObject.unit);
    }

    private double convertToBase() {
        return values * unit.baseValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency that = (Currency) o;
        return convertToBase() == that.convertToBase();
    }
}